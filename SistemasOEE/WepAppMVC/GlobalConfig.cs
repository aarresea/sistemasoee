﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace WebAppMVC
{
    public static class GlobalConfig
    {
        private const string KEY_ROUTE_WEBAPI = "RouteWebApi";
        public static HttpClient WebApiClient = new HttpClient();

        static GlobalConfig()
        {
            WebApiClient.BaseAddress = new Uri(ConfigurationManager.AppSettings[KEY_ROUTE_WEBAPI]);
            WebApiClient.DefaultRequestHeaders.Clear();
            WebApiClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        }
    }
}