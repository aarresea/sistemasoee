﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAppMVC.Models
{
    public class UsuarioModels
    {
        public int IdUsuario { get; set; }

        [Required]
        public string Nombre { get; set; }

        [Required]
        [Range(1, 150)]
        public byte Edad { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Formato email incorrecto")]
        public string Email { get; set; }

        [Required]
        public string Contraseña { get; set; }

        //Adicionales
        public string LoginIncorrecto { get; set; }
    }
}