﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace WebAppMVC.Controllers
{
    public class UsuarioController : Controller
    {
        #region Field
        private const string KEY_DPERSISTENTE = "DPersistente";
        private static bool EsPersistente { get; set; }
        private static WebAppMVC.Models.UsuarioModels usSesion;
        private static List<WebAppMVC.Models.UsuarioModels> listaUsuarios = new List<Models.UsuarioModels>();
        #endregion

        #region Constructor
        public UsuarioController()
        {
            EsPersistente = ConfigurationManager.AppSettings[KEY_DPERSISTENTE].Equals("S");
        }
        #endregion

        #region Public
        public ActionResult Login(FormCollection collection)
        {
            WebAppMVC.Models.UsuarioModels usM;
            if (ModelState.IsValid && collection["inLogin"] != null &&
                collection["inLogin"].ToString().Equals("Login"))
            {
                usM = new WebAppMVC.Models.UsuarioModels()
                {
                    Email = collection["Email"].ToString(),
                    Contraseña = GetSHA1Code(collection["Contraseña"].ToString())
                };

                WebAppMVC.Models.UsuarioModels us;
                if (EsPersistente)
                {
                    var myContent = Newtonsoft.Json.JsonConvert.SerializeObject(usM);
                    var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                    var byteContent = new ByteArrayContent(buffer);
                    byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage response = GlobalConfig.WebApiClient.PostAsync("Usuario/GetLogin", byteContent).Result;
                    us = response.Content.ReadAsAsync<WebAppMVC.Models.UsuarioModels>().Result;
                }
                else
                {
                    us = listaUsuarios.FirstOrDefault(u => u.Email.Equals(usM.Email) && u.Contraseña.Equals(usM.Contraseña));
                }

                if (us != null)
                {
                    usSesion = us;
                    return RedirectToAction("List");
                }
                else
                {
                    usM = new WebAppMVC.Models.UsuarioModels()
                    {
                        LoginIncorrecto = "Datos de login no válidos"
                    };
                    return View(usM);
                }
            }
            else
            {
                usSesion = null;
                usM = new WebAppMVC.Models.UsuarioModels();
                return View(usM);
            }
        }
        public ActionResult List()
        {
            if (usSesion != null)
            {
                IEnumerable<WebAppMVC.Models.UsuarioModels> lUsuario;
                if (EsPersistente)
                {
                    HttpResponseMessage response = GlobalConfig.WebApiClient.GetAsync("Usuario/GetUsuario").Result;
                    lUsuario = response.Content.ReadAsAsync<IEnumerable<WebAppMVC.Models.UsuarioModels>>().Result;
                }
                else
                    lUsuario = listaUsuarios;

                WebAppMVC.Models.UsuarioConectadoModels usCon = new Models.UsuarioConectadoModels()
                {
                    UsuarioConectado = usSesion,
                    ListaUsuarios = lUsuario.ToList()
                };
                return View(usCon);
            }
            else
                return RedirectToAction("Login");
        }
        public ActionResult Create(FormCollection collection)
        {
            if (ModelState.IsValid && collection["inCreate"] != null &&
                collection["inCreate"].ToString().Equals("Create"))
            {
                WebAppMVC.Models.UsuarioModels usM = new WebAppMVC.Models.UsuarioModels()
                {
                    Nombre = collection["Nombre"].ToString(),
                    Edad = byte.Parse(collection["Edad"].ToString()),
                    Email = collection["Email"].ToString(),
                    Contraseña = GetSHA1Code(collection["Contraseña"].ToString())
                };


                bool loginCorrecto = false;
                if (EsPersistente)
                {
                    var myContent = Newtonsoft.Json.JsonConvert.SerializeObject(usM);
                    var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                    var byteContent = new ByteArrayContent(buffer);
                    byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    //HttpContent content = Newtonsoft.Json.JsonConvert.SerializeObject(tipoNota);
                    HttpResponseMessage response = GlobalConfig.WebApiClient.PostAsync("Usuario/PostUsuario", byteContent).Result;
                    loginCorrecto = response.IsSuccessStatusCode;
                }
                else
                {
                    listaUsuarios.Add(usM);
                    loginCorrecto = true;
                }

                if (loginCorrecto)
                    return RedirectToAction("Login");
                else
                    return View();
            }
            else
                return View();
        }
        #endregion

        #region Private
        private static string GetSHA1Code(string str)
        {
            SHA1 sha1 = SHA1Managed.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = sha1.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }
        #endregion
    }
}