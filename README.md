# SistemasOEE

## Descripción de la práctica

La práctica propuesta consiste en realizar una aplicación web de tres pantallas:

1. Alta de Usuarios
2. Ventana de autenticación
3. Listado de Usuarios dados de alta

## Alta de usuarios
Consiste en la creación de nuevos usuarios de la aplicación. Es una pantalla muy sencilla que sólo incluirá cuatro campos:

 - Nombre
 - Edad
 - Email
 - Contraseña
 
La aplicación tendrá dos opciones para el almacenamiento de los usuarios:

1. Persistente: Los usuarios se almacenarán en una base de datos, en archivo o en cualquier otra solución de
persistencia que se elija.

2. Volátil: Los usuarios serán almacenados en memoria (ejemplo, sesión).
Para poder seleccionar el modo de almacenamiento, se dispondrá de una variable de configuración, es decir, la aplicación
trabaja de un modo o de otro. No utilizará los dos de forma simultánea.

### Ventana de autenticación
Esta es la típica ventana de email-contraseña en la que el usuario se puede autenticar. Validará las credenciales
introducidas por el usuario, mostrando los mensajes oportunos de error (las credenciales no son correctas, no se ha
indicado el campo de email o es incorrecto su formato y no se ha indicado el campo de contraseña). 
Si la validación es correcta, se mostrará la ventana de Listado de Usuarios.

### Listado de Usuarios
El listado de usuarios es una pantalla en la que se listan los usuarios dados de alta para comprobar que todo funciona
correctamente.
Se deberá proporcionar un sistema (menú o simples enlaces) para poder navegar entre las distintas ventanas.

## Entorno de la práctica
 
 - Entorno de desarrollo: Visual Studio 2013 y MS SQL Server 2012
 - .NET Framework 4.5
 - Arquitectura 3 capas: 
 	- Frontend (WebAppMVC)
 	- Backend (WebApi)
	- Database (DbSistemasOEE)
 - Entity Framework 6.0
- MVC (Model, View, Controller)
 - Web API
 
## Consideraciones
 
### En el archivo de configuración (Web.Config) del proyecto WebAppMVC
  - Se encuentra el siguiente tag:  
	***<add key="DPersistente" value="S"></add>***  
	Con valor S, la aplicación funciona de manera persistente. Los datos se almacenan e la BBDD. Con otro valor, los datos se almacenan en memoria.  
  - El siguiente tag:  
    	***<add key="RouteWebApi" value="http://localhost:48427/api/"/>***  
    Sirve para configurar la dirección donde se encuentre la api de acceso a datos
 	
### En el archivo de configuración (Web.Config) del proyecto WebApi
- Se encuentra el siguiente tag:  
		  ***<connectionStrings>  
			<add name="DBModel" connectionString="...;data source=SERVIDOR;initial catalog=SistemasOEE;..." />  
		  </connectionStrings>***      
	Donde la etiqueta SERVIDOR, se debe sustituir por nombre/dirección del servidor donde se encuentre la base de datos  
	y en la etiqueta initial catalog, se indicará el nombre de la base de datos.
 	
## Otras consideraciones
- Se ha acotado el alcance a lo solicitado. Se podría:
- Se ha encriptado la contraseña mediante el algoritmo de encriptación SHA1, para no mostrar la contraseña en claro.
- Se indica el nombre del usuario loggeado
- Se añade link de desconexión de sesión (Log out)
- Se ha acotado el desarrollo al alcance solicitado. Futuros evolutivos/mejoras:
 	- Hacer edición/borrado de usuarios
 	- Proteger las conexiones de manera segura, para que los datos se envién de manera segura